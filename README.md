# Export to CSV Bookmarklet #
This is a bookmarklet that, when clicked, will put a link before each table on a web page allowing you to download that table as a CSV file.
